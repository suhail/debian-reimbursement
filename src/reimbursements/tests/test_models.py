from datetime import datetime
from decimal import Decimal

from django.test import SimpleTestCase, TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group

from reimbursements.models import ExpenseType, Request, RequestHistory, RequestType


class TestRequestType(TestCase):
    def test_expense_type_map(self):
        travel = RequestType.objects.create(name="Travel")
        hardware = RequestType.objects.create(name="Hardware")
        accomm = ExpenseType.objects.create(name="Accommodation")
        accomm.request_types.set([travel])
        other = ExpenseType.objects.create(name="Other")
        other.request_types.set([travel, hardware])
        self.assertEqual(
            RequestType.expense_type_map(),
            {
                accomm.id: [travel.id, other.id],
                hardware.id: [other.id],
            },
        )


class TestRequestSimpleMethods(SimpleTestCase):
    def test_subject_multiline(self):
        request = Request(description="Line 1\nLine 2\nLine 3")
        self.assertEqual(request.subject, "Line 1")

    def test_subject_oneline(self):
        request = Request(description="Line 1")
        self.assertEqual(request.subject, "Line 1")

    def test_subject_empty(self):
        request = Request(description="")
        self.assertEqual(request.subject, "")


class TestEmptyRequest(TestCase):
    def setUp(self):
        User = get_user_model()
        self.user = User.objects.create_user(username="user")
        self.approvers = Group.objects.create(name="approvers")
        self.payers = Group.objects.create(name="payers")
        self.travel = RequestType.objects.create(name="Travel")
        self.accomm = ExpenseType.objects.create(name="Accommodation")
        self.accomm.request_types.set([self.travel])
        self.request = Request.objects.create(
            requester=self.user,
            request_type=self.travel,
            description="A Test Request",
            currency="USD",
            approver_group=self.approvers,
            payer_group=self.payers,
        )
        RequestHistory.objects.create(
            request=self.request,
            actor=self.user,
            event=RequestHistory.Events.CREATED,
        )

    def test_total(self):
        self.assertEqual(self.request.total, (Decimal(0), "USD"))

    def test_receipts_total(self):
        self.assertEqual(self.request.receipts_total, (Decimal(0), "USD"))

    def test_reimbursed_total(self):
        self.assertEqual(self.request.reimbursed_total, (Decimal(0), "USD"))

    def test_approved_budget_limit(self):
        self.assertEqual(self.request.approved_budget_limit, (Decimal(0), "USD"))

    def test_total_reimburseable(self):
        self.assertEqual(self.request.total_reimburseable, (Decimal(0), "USD"))

    def test_within_budget(self):
        self.assertEqual(self.request.within_budget, True)

    def test_over_budget(self):
        self.assertEqual(self.request.over_budget, False)

    def test_created(self):
        self.assertIsInstance(self.request.created, datetime)

    def test_updated(self):
        self.assertEqual(self.request.created, self.request.updated)

    def test_absolute_url(self):
        self.assertEqual(
            self.request.get_absolute_url(), f"/requests/{self.request.id}"
        )

    def test_fully_qualified_url(self):
        self.assertEqual(
            self.request.get_fully_qualified_url(),
            f"https://reimbursements.example.com/requests/{self.request.id}",
        )

    def test_summarize_by_type(self):
        summary = self.request.summarize_by_type()
        self.assertIsInstance(summary, dict)
        self.assertEqual(list(summary.keys()), ["total"])
        total = summary["total"]
        self.assertTrue(total.special)
        self.assertEqual(total.name, "Total")
        self.assertEqual(total.currency, "USD")
        self.assertEqual(total.spent, Decimal(0))
        self.assertEqual(total.reimbursed, Decimal(0))

    def test_summarize_by_type_dict(self):
        self.assertEqual(
            self.request.summarize_by_type_dict(),
            {
                "Total": {
                    "currency": "USD",
                    "reimbursed": 0.0,
                    "requested": 0.0,
                    "spent": 0.0,
                },
            },
        )

    def test_user_access_requester(self):
        self.assertEqual(self.request.user_access(self.user), {"requester": True})

    def test_user_access_approver(self):
        User = get_user_model()
        user2 = User.objects.create_user(username="user2")
        user2.groups.set([self.approvers])
        self.assertEqual(self.request.user_access(user2), {"approver": True})

    def test_user_access_payer(self):
        User = get_user_model()
        user2 = User.objects.create_user(username="user2")
        user2.groups.set([self.payers])
        self.assertEqual(self.request.user_access(user2), {"payer": True})

    def test_user_access_unrelated(self):
        User = get_user_model()
        user2 = User.objects.create_user(username="user2")
        self.assertEqual(self.request.user_access(user2), {})
